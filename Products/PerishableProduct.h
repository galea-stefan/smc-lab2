#pragma once
#include "Product.h"
class PerishableProduct : public Product
{
public:
	PerishableProduct(uint16_t _id, std::string _name, float _price, uint16_t _vat, std::string _expDate);
	std::string GetExpirationDate() const;

private:
	std::string expirationDate;

};

