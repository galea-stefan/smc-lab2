#pragma once
#include <string>
#include <stdint.h>
#include <fstream>
#include "IPriceable.h"


class Product : IPriceable
{
public:
	Product(uint16_t, std::string, float, uint16_t);
	Product(const Product& other) = default;
	~Product() = default;

	uint16_t GetProductId() const;
	std::string GetName() const;
	float GetPrice() const override;
	uint16_t GetVAT() const override;

protected:
	uint16_t productId;
	std::string name;
	float price;
	uint16_t vat;
};
