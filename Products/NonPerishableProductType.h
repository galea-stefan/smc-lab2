#pragma once

enum class NonPerishableProductType
{
	PersonalHygiene,
	SmallAppliences,
	Clothing
};