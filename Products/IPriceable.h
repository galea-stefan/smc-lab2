#pragma once

class IPriceable
{
public:
	virtual float GetPrice() const = 0;
	virtual uint16_t GetVAT() const = 0;
};