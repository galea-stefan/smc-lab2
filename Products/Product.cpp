#include "Product.h"

Product::Product(uint16_t _id, std::string _name, float _price, uint16_t _tva) 
	: productId(_id),
	name(_name),
	price(_price),
	vat(_tva)
{
}


uint16_t Product::GetProductId() const
{
	return productId;
}

std::string Product::GetName() const
{
	return name;
}

float Product::GetPrice() const
{
	return price;
}

uint16_t Product::GetVAT() const
{
	return vat;
}