#include "NonPerishableProduct.h"

NonPerishableProduct::NonPerishableProduct(uint16_t _id, std::string _name, float _price, uint16_t _vat, NonPerishableProductType _type) :
	Product(_id, _name, _price, _vat),
	productType(_type)
{
}

NonPerishableProductType NonPerishableProduct::GetProductType() const
{
	return productType;
}
