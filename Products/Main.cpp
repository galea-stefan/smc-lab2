#include <vector>
#include <regex>
#include <unordered_map>
#include <iostream>

#include "PerishableProduct.h"
#include "NonPerishableProduct.h"
#include "NonPerishableProductType.h"

int main()
{
	std::vector<Product*> products;

	const std::regex dateRegex("\\d{4}-\\d{2}-\\d{2}");
	const std::unordered_map<std::string, NonPerishableProductType> enumStrings
	{
		{"PersonalHygiene", NonPerishableProductType::PersonalHygiene},
		{"SmallAppliences", NonPerishableProductType::SmallAppliences},
		{"Clothing", NonPerishableProductType::Clothing}
	};

	for (std::ifstream file("data.txt");!file.eof();)
	{
		uint16_t productId;
		file >> productId;

		std::string name;
		file >> name;

		float price;
		file >> price;

		uint16_t vat;
		file >> vat;

		std::string dateOrCateg;
		file >> dateOrCateg;

		std::string expirationDate;
		NonPerishableProductType productCategory;

		if (std::regex_match(dateOrCateg, dateRegex))
		{
			expirationDate = dateOrCateg;
			products.push_back(new PerishableProduct(productId, name, price, vat, expirationDate));
		}
		else
		{
			productCategory = enumStrings.at(dateOrCateg);
			products.push_back(new NonPerishableProduct(productId, name, price, vat, productCategory));
		}
	}

	/*std::sort(products.begin(), products.end(), [](const Product& p1, const Product& p2) {p1.GetName() < p2.GetName()});
	std::sort(products.begin(), products.end(), [](const Product& p1, const Product& p2) {p1.GetName() < p2.GetName()});*/

	for (auto product : products)
	{
		std::cout << product->GetProductId() << " " << product->GetName() << "\n";
	}
}