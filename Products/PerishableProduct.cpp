#include "PerishableProduct.h"

PerishableProduct::PerishableProduct(uint16_t _id, std::string _name, float _price, uint16_t _vat, std::string _expDate) :
	Product(_id, _name, _price, _vat),
	expirationDate(_expDate)
{
}

std::string PerishableProduct::GetExpirationDate() const
{
	return expirationDate;
}