#pragma once
#include "Product.h"
#include "NonPerishableProductType.h"

class NonPerishableProduct : public Product
{
public:
	NonPerishableProduct(uint16_t _id, std::string _name, float _price, uint16_t _vat, NonPerishableProductType _type);
	NonPerishableProductType GetProductType() const;

private:
	NonPerishableProductType productType;
};

